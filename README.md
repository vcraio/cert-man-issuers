# cert-man-issuers

### What is this?
A helm chart to load Issuers and Cluster issuers into a k8s Cluster that has [cert-manager](https://cert-manager.io/) 
installed

It currently supports the following Issuers:
* ACME

To use:

Install this helm chart with a values.yaml that looks something like this:

```yaml
issuers:
  - name: test
    namespace: test # Only valid for Issuers
    type: ClusterIssuer # Must be one of Issuer or ClusterIssuer
    acme:
      email: test@test.com
      server: https://acme-staging-v02.api.letsencrypt.org/directory
      solvers:
        - http01:
            ingress:
              class: "nginx"
```



